package net.creativious.creativiouslibrary.lib;

import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(CreativiousLib.MOD_ID)
public class CreativiousLib {
    public static final String MOD_ID = "creativiouslib";
    public static final String MOD_NAME = "Creativious Library";

    public static final Logger LOGGER = LogManager.getLogger(MOD_NAME);

    private static CreativiousLib INSTANCE;

    public CreativiousLib() {
        INSTANCE = this;
    }
}
